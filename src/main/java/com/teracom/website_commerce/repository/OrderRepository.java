package com.teracom.website_commerce.repository;

import com.teracom.website_commerce.common.model.TopUser;
import com.teracom.website_commerce.model.Brand;
import com.teracom.website_commerce.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface OrderRepository extends JpaRepository<Order, Integer> {
    @Query(value = "select u from Order u where u.customer.name like CONCAT('%',?1,'%')")
    List<Order> searchOrder(String query);

    @Modifying
    @Query(value = "update orders set status = 0 where id = ?1", nativeQuery = true)
    void softDelete(Integer id);

    @Query(name="topUser",nativeQuery = true)
    List<TopUser> topUser();
}

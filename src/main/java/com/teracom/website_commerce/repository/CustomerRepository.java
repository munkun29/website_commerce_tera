package com.teracom.website_commerce.repository;

import com.teracom.website_commerce.model.Customer;
import com.teracom.website_commerce.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer>, JpaSpecificationExecutor<Customer> {
    @Query(value = "select c from Customer c")
    List<Customer> getListCustomer(Pageable pageable);
    Customer getCustomerByUser (User user);
    List<Customer> findAll();
}

package com.teracom.website_commerce.repository;

import com.teracom.website_commerce.model.Brand;
import com.teracom.website_commerce.model.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public interface ProductRepository extends JpaRepository<Product, Integer> {

    @Query(value = "select * from product u where u.name like CONCAT('%',?1,'%')",nativeQuery = true)
    List<Product> searchProduct(String query);

    @Modifying
    @Query(value = "update product set status = 0 where id = ?1", nativeQuery = true)
    void softDelete(Integer id);

    List<Product> findAllByOrderByPriceAsc();

    List<Product> findAllByStatusIsTrueOrderByPriceAsc();

    Page<Product> findByStatusTrue(Pageable pageable);

    Optional<Product> findProductByIdAndStatusTrue(Integer id);
}

package com.teracom.website_commerce.repository;

import com.teracom.website_commerce.model.Brand;
import com.teracom.website_commerce.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface CategoryRepository extends JpaRepository<Category, Integer> {

    @Query(value = "select * from category u where u.name like CONCAT('%',?1,'%')",nativeQuery = true)
    List<Category> searchCategory(String query);

    @Modifying
    @Query(value = "update category set status = 0 where id = ?1", nativeQuery = true)
    void softDelete(Integer id);

    boolean existsCategoriesByName(String name);

}

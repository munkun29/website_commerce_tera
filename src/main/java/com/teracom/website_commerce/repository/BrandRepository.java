package com.teracom.website_commerce.repository;

import com.teracom.website_commerce.model.Brand;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public interface BrandRepository extends JpaRepository<Brand, Integer> {
    @Query(value = "select * from brand u where u.name like CONCAT('%',?1,'%') and status = 1",nativeQuery = true)
    List<Brand> searchBrand(String query);

    @Modifying
    @Query(value = "update brand set status = 0 where id = ?1", nativeQuery = true)
    void softDelete(Integer id);

    boolean existsBrandByName(String name);

    Page<Brand> findByStatusTrue(Pageable pageable);

    Optional<Brand> findBrandByIdAndStatusTrue(Integer id);
}

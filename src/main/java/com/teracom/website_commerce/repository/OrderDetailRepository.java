package com.teracom.website_commerce.repository;

import com.teracom.website_commerce.common.model.TopProduct;
import com.teracom.website_commerce.model.OrderDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface OrderDetailRepository extends JpaRepository<OrderDetail, Integer> {
    @Query(name = "topProduct",nativeQuery = true)
    List<TopProduct> listProduct();
    @Query(name = "lowProduct",nativeQuery=true)
    List<TopProduct> sellSlowlyProduct();

    @Query("select u from OrderDetail u where u.order.id = ?1")
    List<OrderDetail> findOrderDetailByOrderID(Integer id);
}


package com.teracom.website_commerce.common.controller;

import com.teracom.website_commerce.common.service.StorageService;
import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.UUID;

@Service
public class ImageStorageImage implements StorageService {

    private final Path storageFolder = Paths.get("uploads");

    public ImageStorageImage() {
        try {
            Files.createDirectories(storageFolder);
        } catch(IOException e) {
            throw new RuntimeException("cannot initalize storage,"+e);
        }
    }

    private boolean isImageFile(MultipartFile file) {
        String fileExtension = FilenameUtils.getExtension(file.getOriginalFilename());
        return Arrays.asList(new String[] {"png", "jpg"})
                .contains(fileExtension.trim().toLowerCase());
    }

    @Override
    public String storeFile(MultipartFile file) {
        try {
            System.out.println("haha");
            if(file.isEmpty()){
                throw new RuntimeException("failed to store empty file");
            }
            if(!isImageFile(file)){
                throw new RuntimeException("you can only upload image file");
            }
            float fileSizeInMegabytes = file.getSize()/1_000_000;
            if(fileSizeInMegabytes > 5.0f) {
                throw new RuntimeException("file must <5 mb");
            }
            String fileExtension = FilenameUtils.getExtension(file.getOriginalFilename());
            String generatedFileName = UUID.randomUUID().toString().replace("-","");
            generatedFileName = generatedFileName + "." + fileExtension;
            Path destinationFilePath = this.storageFolder.resolve(
                            Paths.get(generatedFileName))
                    .normalize().toAbsolutePath();
            if(!destinationFilePath.getParent().equals(this.storageFolder.toAbsolutePath())) {
                throw new RuntimeException(
                        "Cannot store file outside current directory"
                );
            }
            try (InputStream inputStream = file.getInputStream()) {
                Files.copy(inputStream, destinationFilePath, StandardCopyOption.REPLACE_EXISTING);
            }
            return generatedFileName;
        } catch(IOException e) {
            throw new RuntimeException("failed to store file."+e);
        }
    }
}

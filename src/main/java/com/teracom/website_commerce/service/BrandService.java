package com.teracom.website_commerce.service;

import com.teracom.website_commerce.model.Brand;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;


public interface BrandService {
    List<Brand> findAll();

    <S extends Brand> S saveAndFlush(S entity);

    Page<Brand> findAll(Pageable pageable);

    <S extends Brand> S save(S entity);

    Optional<Brand> findById(Integer integer);

    boolean existsById(Integer integer);

    long count();

    void deleteById(Integer integer);

    void delete(Brand entity);

    void deleteAll();

    @Query(value = "select * from brand u where u.name like CONCAT('%',?1,'%')", nativeQuery = true)
    List<Brand> searchBrand(String query);

    @org.springframework.data.jpa.repository.Modifying
    @Query(value = "update brand set status = 0 where id = ?1", nativeQuery = true)
    void softDelete(Integer id);

    boolean existsBrandByName(String name);

    Page<Brand> findByStatusTrue(Pageable pageable);

    Optional<Brand> findBrandByIdAndStatusTrue(Integer id);
}

package com.teracom.website_commerce.service;

import com.teracom.website_commerce.model.Category;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface CategoryService {
    List<Category> findAll();

    List<Category> findAll(Sort sort);

    List<Category> findAllById(Iterable<Integer> integers);

    <S extends Category> S save(S entity);

    Optional<Category> findById(Integer integer);

    boolean existsById(Integer integer);

    long count();

    void deleteById(Integer integer);

    void delete(Category entity);

    void deleteAll();

    Page<Category> findAll(Pageable pageable);

    @Query(value = "select * from category u where u.name like CONCAT('%',?1,'%')", nativeQuery = true)
    List<Category> searchCategory(String query);

    @Query(value = "update category set status = 0 where id = ?1", nativeQuery = true)
    @Modifying
    void softDelete(Integer id);

    boolean existsCategoriesByName(String name);
}

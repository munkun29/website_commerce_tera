package com.teracom.website_commerce.service;

import com.teracom.website_commerce.dto.request.CustomerForm;
import com.teracom.website_commerce.dto.request.CustomerUpdateForm;
import com.teracom.website_commerce.dto.response.CustomerDTO;
import com.teracom.website_commerce.dto.response.ListCustomer;
import com.teracom.website_commerce.model.Customer;
import com.teracom.website_commerce.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.IOException;

public interface CustomerService {
    CustomerDTO create (CustomerForm form) throws IOException;
    ListCustomer list(int page, int size) ;
    CustomerDTO update (CustomerUpdateForm form);
    String delete(Integer id);
    CustomerDTO detail (Integer id);

    Customer getCustomer(User user);
}

package com.teracom.website_commerce.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.teracom.website_commerce.common.model.TopUser;
import com.teracom.website_commerce.model.Order;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.FluentQuery;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

public interface OrderService {


    Order create(JsonNode orderData);

    @Query("select u from Order u where u.customer.name like CONCAT('%',?1,'%')")
    List<Order> searchOrder(String query);

    @Query(value = "update orders set status = 0 where id = ?1", nativeQuery = true)
    @Modifying
    void softDelete(Integer id);

    List<Order> findAll();

    List<Order> findAll(Sort sort);

    List<Order> findAllById(Iterable<Integer> integers);

    <S extends Order> List<S> saveAll(Iterable<S> entities);

    void flush();

    <S extends Order> S saveAndFlush(S entity);

    <S extends Order> List<S> saveAllAndFlush(Iterable<S> entities);

    @Deprecated
    void deleteInBatch(Iterable<Order> entities);

    void deleteAllInBatch(Iterable<Order> entities);

    void deleteAllByIdInBatch(Iterable<Integer> integers);

    void deleteAllInBatch();

    @Deprecated
    Order getOne(Integer integer);

    @Deprecated
    Order getById(Integer integer);

    Order getReferenceById(Integer integer);

    <S extends Order> List<S> findAll(Example<S> example);

    <S extends Order> List<S> findAll(Example<S> example, Sort sort);

    Page<Order> findAll(Pageable pageable);

    <S extends Order> S save(S entity);

    Optional<Order> findById(Integer integer);

    boolean existsById(Integer integer);

    long count();

    void deleteById(Integer integer);

    void delete(Order entity);

    void deleteAllById(Iterable<? extends Integer> integers);

    void deleteAll(Iterable<? extends Order> entities);

    void deleteAll();

    <S extends Order> Optional<S> findOne(Example<S> example);

    <S extends Order> Page<S> findAll(Example<S> example, Pageable pageable);

    <S extends Order> long count(Example<S> example);

    <S extends Order> boolean exists(Example<S> example);

    <S extends Order, R> R findBy(Example<S> example, Function<FluentQuery.FetchableFluentQuery<S>, R> queryFunction);

    @Query(name = "topUser", nativeQuery = true)
    List<TopUser> topUser();
}

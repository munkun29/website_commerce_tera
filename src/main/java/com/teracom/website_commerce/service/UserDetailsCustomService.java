package com.teracom.website_commerce.service;

import com.teracom.website_commerce.model.User;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserDetailsCustomService extends UserDetailsService {
    User load(String email);
    User loadUserByEmail(String email);

}

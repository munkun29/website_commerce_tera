package com.teracom.website_commerce.service;

import com.teracom.website_commerce.common.model.TopProduct;
import com.teracom.website_commerce.model.OrderDetail;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface OrderDetailService {
    List<OrderDetail> findAll();

    List<OrderDetail> findAll(Sort sort);

    <S extends OrderDetail> S saveAndFlush(S entity);

    Page<OrderDetail> findAll(Pageable pageable);

    <S extends OrderDetail> S save(S entity);

    Optional<OrderDetail> findById(Integer integer);

    boolean existsById(Integer integer);

    long count();

    void deleteById(Integer integer);

    void delete(OrderDetail entity);

    @Query(name = "topProduct", nativeQuery = true)
    List<TopProduct> listProduct();

    @Query(name = "lowProduct", nativeQuery = true)
    List<TopProduct> sellSlowlyProduct();
}

package com.teracom.website_commerce.service;

import com.teracom.website_commerce.dto.Token;
import com.teracom.website_commerce.dto.request.RegisterForm;
import com.teracom.website_commerce.model.User;

import javax.servlet.http.HttpServletRequest;

public interface UserService {
    Token register(HttpServletRequest request ,RegisterForm form);
    User getCurrentUser ();
    String delete (int id);
}

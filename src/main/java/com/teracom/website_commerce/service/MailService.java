package com.teracom.website_commerce.service;

import com.teracom.website_commerce.dto.request.MailForm;

import java.io.IOException;

public interface MailService {
    void sendHtml(MailForm mail);
    void sendmailAccount (String email, String password) throws IOException;
}

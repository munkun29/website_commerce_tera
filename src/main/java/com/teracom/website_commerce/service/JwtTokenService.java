package com.teracom.website_commerce.service;

import com.teracom.website_commerce.dto.request.InfoLogin;
import com.teracom.website_commerce.model.User;
import io.jsonwebtoken.Claims;

import javax.servlet.http.HttpServletRequest;
import java.util.function.Function;

public interface JwtTokenService {
    default <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver){
        return claimsResolver.apply(getAllClaimsFromToken(token));
    }
    String getTokenIfExist(HttpServletRequest request, InfoLogin infoLogin);
    String generateToken(HttpServletRequest request,InfoLogin infoLogin);
    String generateToken(HttpServletRequest request, User user);
    Claims getAllClaimsFromToken(String token);
    boolean validateToken(HttpServletRequest request, String token, String email);
    boolean isValid(HttpServletRequest request, String token);
    String getEmailFromJwtToken(String token);
    String getRoleFromJwtToken (String token);
    String getIpFromToken (String token);
}

package com.teracom.website_commerce.service;

import com.teracom.website_commerce.model.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ProductService {
    List<Product> findAll();

    List<Product> findAll(Sort sort);

    Page<Product> findAll(Pageable pageable);

    <S extends Product> S save(S entity);

    Optional<Product> findById(Integer integer);

    boolean existsById(Integer integer);

    long count();

    void deleteById(Integer integer);

    void delete(Product entity);

    void deleteAll();

    @Query(value = "select * from product u where u.name like CONCAT('%',?1,'%')", nativeQuery = true)
    List<Product> searchProduct(String query);

    @Query(value = "update product set status = 0 where id = ?1", nativeQuery = true)
    @Modifying
    void softDelete(Integer id);

    List<Product> findAllByOrderByPriceAsc();

    Page<Product> findByStatusTrue(Pageable pageable);

    Optional<Product> findProductByIdAndStatusTrue(Integer id);

    List<Product> findAllByStatusIsTrueOrderByPriceAsc();
}

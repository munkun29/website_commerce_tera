package com.teracom.website_commerce.service.impl;

import com.teracom.website_commerce.cache.JwtTokenCache;
import com.teracom.website_commerce.cache.UserLoginCache;
import com.teracom.website_commerce.constant.JwtClaimKey;
import com.teracom.website_commerce.dto.request.InfoLogin;
import com.teracom.website_commerce.exception.AuthException;
import com.teracom.website_commerce.exception.enumarate.AuthError;
import com.teracom.website_commerce.model.User;
import com.teracom.website_commerce.repository.UserRepository;
import com.teracom.website_commerce.service.JwtTokenService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import io.jsonwebtoken.*;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Log4j2
@Service
@RequiredArgsConstructor
public class JwtTokenServiceImpl implements JwtTokenService {
    @Value("${jwt.secret}")
    private String jwtKey ;
    @Value("${jwt.expirationDateInDay}")
    private long expDay;

    private final JwtTokenCache jwtCache;
    private final UserLoginCache userLoginCache;
    private final UserRepository userRepository;

    @Override
    public String getTokenIfExist(HttpServletRequest request, InfoLogin infoLogin) {
        String token = null;
        final InfoLogin history = userLoginCache.get(infoLogin.getEmail());
        final String password = history == null ? null : history.getPassword();
        if (infoLogin.getPassword().equals(password)) {
            token = jwtCache.get(infoLogin.getEmail());
            log.info("Found token ---> user:{}, token: {}", infoLogin.getEmail(), token);
        }
        if (isValid(request,token)) {
            token = null;
        }
        return token;
    }
    private String doGenerateToken(Map<String, Object> claims, User userDetail) {
        final long expMs = expDay * 24 * 3600 * 60 * 1000;
        return Jwts.builder()
                .setId(userDetail.getId().toString())
                .setHeaderParam("typ", "JWT")
                .setClaims(claims)
                .setSubject(userDetail.getEmail())
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + expMs))
                .signWith(SignatureAlgorithm.HS256, jwtKey)
                .compact();
    }

    @Override
    public String generateToken(HttpServletRequest request,InfoLogin infoLogin) {
        Map<String, Object> claims = new HashMap<>();
        User user = userRepository.findByEmail(infoLogin.getEmail());
        claims.put(JwtClaimKey.ROLE, user.getRole());
        claims.put(JwtClaimKey.USER_ID, user.getId());
        claims.put("ip", getClientIp(request));
        String token = doGenerateToken(claims, user);
        userLoginCache.put(infoLogin.getEmail(), infoLogin);
        jwtCache.put(infoLogin.getEmail(), token);
        log.info("Generate jwt-token by user {}: {}", infoLogin.getEmail(), token);
        return token;
    }

    @Override
    public String generateToken(HttpServletRequest request, User user) {
        Map<String, Object> claims = new HashMap<>();
        claims.put(JwtClaimKey.ROLE, user.getRole());
        claims.put(JwtClaimKey.USER_ID, user.getId());
        claims.put("ip", getClientIp(request));
        return doGenerateToken(claims, user);
    }

    @Override
    public Claims getAllClaimsFromToken(String token) {
        try {
            return Jwts.parser().setSigningKey(jwtKey).parseClaimsJws(token).getBody();
        } catch (ExpiredJwtException e) {
            log.error("Token has expired: {}", token);
            throw new AuthException(AuthError.INVALID_TOKEN);
        } catch (SignatureException ex) {
            log.error("JWT signature does not match locally computed signature: {}", token);
            throw new AuthException(AuthError.INVALID_TOKEN);
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            throw new AuthException(AuthError.UNAUTHORIZED);
        }
    }

    @Override
    public boolean validateToken(HttpServletRequest request, String token, String email) {
        final String inside = getClaimFromToken(token, Claims::getSubject);
        return (inside.equals(email) && !isValid(request,token));
    }

    @Override
    public boolean isValid(HttpServletRequest request, String token) {
        final String ipAddress = getClientIp(request);
        if (!ipAddress.equals(getIpFromToken(token))){
            return true;
        }
        if (token == null)
            return true;
        final Date expiration = getClaimFromToken(token, Claims::getExpiration);
        if (expiration == null)
            return true;
        return expiration.before(new Date());
    }

    @Override
    public String getEmailFromJwtToken(String token) {
        return Jwts.parser()
                .setSigningKey(jwtKey)
                .parseClaimsJws(token)
                .getBody()
                .getSubject();
    }

    @Override
    public String getRoleFromJwtToken(String token) {
        return  Jwts.parser()
                .setSigningKey(jwtKey)
                .parseClaimsJws(token)
                .getBody()
                .get("role").toString();
    }


    private static String getClientIp(HttpServletRequest request) {
        String remoteAddr = "";
        if (request != null) {
            remoteAddr = request.getHeader("X-FORWARDED-FOR");
            if (remoteAddr == null || "".equals(remoteAddr)) {
                remoteAddr = request.getRemoteAddr();
            }
        }
        return remoteAddr;
    }
    public String getIpFromToken (String token) {
        return Jwts.parser()
                .setSigningKey(jwtKey)
                .parseClaimsJws(token)
                .getBody().get("ip").toString();
    }
}

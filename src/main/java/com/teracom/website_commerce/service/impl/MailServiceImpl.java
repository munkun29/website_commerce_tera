package com.teracom.website_commerce.service.impl;

import com.teracom.website_commerce.dto.request.MailForm;
import com.teracom.website_commerce.service.MailService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ResourceLoader;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
@Log4j2
@RequiredArgsConstructor
public class MailServiceImpl implements MailService {
    @Value("${spring.mail.username}")
    private String mailFrom;

    @Value("${spring.mail.password}")
    private String password;

    @Value("${spring.mail.host}")
    private String host;

    @Value("${spring.mail.port}")
    private String port;

    private final  JavaMailSender mailSender;
    private final ExecutorService poolSender = Executors.newSingleThreadExecutor();
    private final ResourceLoader resourceLoader;

    @Override
    public void sendHtml(MailForm mail) {
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.port", port);
        try {
            JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
            mailSender.setPassword(password);
            mailSender.setUsername(mailFrom);
            mailSender.setJavaMailProperties(props);
            MimeMessage mimeMessage = mailSender.createMimeMessage();
            MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
            mimeMessageHelper.setSubject(mail.getSubject());
            mimeMessageHelper.setFrom(new InternetAddress(mailFrom, "Commerce-tera"));
            mimeMessageHelper.setTo(mail.getMailTo());
            mimeMessageHelper.setText(mail.getContent(), true);
            poolSender.submit(() -> mailSender.send(mimeMessage));
        } catch (Exception ex) {
            log.error("Failed to send mail: {}", ex.getMessage(), ex);
        }
    }

    @Override
    public void sendmailAccount(String email, String password) throws IOException {
        Path path = Paths.get(resourceLoader.getResource("classpath:static.template/create-account.html").getURI());
        String content = new String(Files.readAllBytes(path), StandardCharsets.UTF_8);
        String mailContent = content.replace("{{password}}", password);
        MailForm mail = new MailForm();
        mail.setMailTo(email);
        mail.setSubject("Your account");
        mail.setContent(mailContent);
        sendHtml(mail);
    }
}

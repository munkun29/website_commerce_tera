package com.teracom.website_commerce.service.impl;
import com.teracom.website_commerce.model.User;
import com.teracom.website_commerce.repository.UserRepository;
import com.teracom.website_commerce.service.UserDetailsCustomService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


@Log4j2
@Service
@RequiredArgsConstructor
public class UserDetailServiceImpl implements UserDetailsCustomService {
    private final UserRepository usersRepository;


    @Override
    public User loadUserByEmail(String email) throws UsernameNotFoundException {
        final User user = load(email);

        log.info("Load user ne-------> {}", user);

        return user;
    }

    @Override
    public User load(String email) {
        return  usersRepository.findByEmail(email);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final User user = load(username);

        log.info("Load user ne-------> {}", user, username);

        return user;
    }
}

package com.teracom.website_commerce.service.impl;

import com.teracom.website_commerce.model.Brand;
import com.teracom.website_commerce.repository.BrandRepository;
import com.teracom.website_commerce.service.BrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BrandServiceImpl implements BrandService {

    @Autowired
    private BrandRepository brandRepository;

    @Override
    public List<Brand> findAll() {
        return brandRepository.findAll();
    }

    @Override
    public <S extends Brand> S saveAndFlush(S entity) {
        return brandRepository.saveAndFlush(entity);
    }

    @Override
    public Page<Brand> findAll(Pageable pageable) {
        return brandRepository.findAll(pageable);
    }

    @Override
    public <S extends Brand> S save(S entity) {
        return brandRepository.save(entity);
    }

    @Override
    public Optional<Brand> findById(Integer integer) {
        return brandRepository.findById(integer);
    }

    @Override
    public boolean existsById(Integer integer) {
        return brandRepository.existsById(integer);
    }

    @Override
    public long count() {
        return brandRepository.count();
    }

    @Override
    public void deleteById(Integer integer) {
        brandRepository.deleteById(integer);
    }

    @Override
    public void delete(Brand entity) {
        brandRepository.delete(entity);
    }

    @Override
    public void deleteAll() {
        brandRepository.deleteAll();
    }

    @Override
    @Query(value = "select * from brand u where u.name like CONCAT('%',?1,'%')", nativeQuery = true)
    public List<Brand> searchBrand(String query) {
        return brandRepository.searchBrand(query);
    }

    @Override
    @org.springframework.data.jpa.repository.Modifying
    @Query(value = "update brand set status = 0 where id = ?1", nativeQuery = true)
    public void softDelete(Integer id) {
        brandRepository.softDelete(id);
    }

    @Override
    public boolean existsBrandByName(String name) {
        return brandRepository.existsBrandByName(name);
    }

    public Page<Brand> findByStatusTrue(Pageable pageable) {
        return brandRepository.findByStatusTrue(pageable);
    }

    @Override
    public Optional<Brand> findBrandByIdAndStatusTrue(Integer id) {
        return brandRepository.findBrandByIdAndStatusTrue(id);
    }
}

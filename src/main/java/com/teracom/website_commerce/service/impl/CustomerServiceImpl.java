package com.teracom.website_commerce.service.impl;

import com.teracom.website_commerce.dto.request.CustomerForm;
import com.teracom.website_commerce.dto.request.CustomerUpdateForm;
import com.teracom.website_commerce.dto.response.CustomerDTO;
import com.teracom.website_commerce.dto.response.ListCustomer;
import com.teracom.website_commerce.exception.CustomerException;
import com.teracom.website_commerce.exception.UserException;
import com.teracom.website_commerce.exception.enumarate.CustomerError;
import com.teracom.website_commerce.exception.enumarate.UserError;
import com.teracom.website_commerce.model.Customer;
import com.teracom.website_commerce.model.User;
import com.teracom.website_commerce.repository.CustomerRepository;
import com.teracom.website_commerce.repository.UserRepository;
import com.teracom.website_commerce.service.CustomerService;
import com.teracom.website_commerce.service.MailService;
import com.teracom.website_commerce.utils.ExtendedBeanUtils;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@Service
@Log4j2
public class CustomerServiceImpl implements CustomerService {
    private final UserRepository userRepository;
    private final CustomerRepository customerRepository;
    private final PasswordEncoder passwordEncoder;
    private final MailService mailService;
    private final UserServiceImpl userService;
    private final JwtTokenServiceImpl jwtTokenService;

    public CustomerServiceImpl(UserRepository userRepository, CustomerRepository customerRepository, PasswordEncoder passwordEncoder, MailService mailService, UserServiceImpl userService, JwtTokenServiceImpl jwtTokenService) {
        this.userRepository = userRepository;
        this.customerRepository = customerRepository;
        this.passwordEncoder = passwordEncoder;
        this.mailService = mailService;
        this.userService = userService;
        this.jwtTokenService = jwtTokenService;
    }

    @Override
    public CustomerDTO create(CustomerForm form) throws IOException {
        if(userRepository.existsByEmail(form.getEmail()))
        {
            throw new UserException(UserError.USER_EXISTS);
        }
        Customer customer = new Customer();
        BeanUtils.copyProperties(form, customer);
        customerRepository.save(customer);

        User user = new User();
        //Random password
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(8);
        for (int i = 0; i < 8; i++) {
            int randomLimitedInt = leftLimit + (int)
                    (random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }
        String password = buffer.toString();
        user.setPassword(passwordEncoder.encode(password));
        //Send mail
        user.setEmail(form.getEmail());
        mailService.sendmailAccount(form.getEmail(), password);

        userRepository.save(user);
        customer.setUser(user);
        customerRepository.save(customer);

        CustomerDTO customerDTO = new CustomerDTO();
        BeanUtils.copyProperties(customer, customerDTO);
        return customerDTO;
    }

    @Override
    public ListCustomer list(int page, int size) {
        ListCustomer listCustomer = new ListCustomer();
        List<CustomerDTO> listCustomerDTO = new ArrayList<>();
        Pageable pageable = PageRequest.of(page, size);
        List<Customer> list = customerRepository.getListCustomer(pageable);
        for (Customer customer:list) {
        CustomerDTO customerDTO = new CustomerDTO();
        BeanUtils.copyProperties(customer,customerDTO);
        listCustomerDTO.add(customerDTO);
        }
        int totalItem = customerRepository.findAll().size();
        listCustomer.setCustomerList(listCustomerDTO);
        listCustomer.setTotalItem(totalItem);
        return listCustomer;
    }

    @Override
    public CustomerDTO update(CustomerUpdateForm form) {
        User user = userService.getCurrentUser();
        Customer customer = customerRepository.getCustomerByUser(user);
        ExtendedBeanUtils.copyPropertiesIgnoreNull(form,customer);
        customerRepository.save(customer);
        CustomerDTO customerDTO = new CustomerDTO();
        BeanUtils.copyProperties(customer,customerDTO);
        return customerDTO;
    }

    @Override
    public String delete(Integer id) {
        Optional<Customer> customer = customerRepository.findById(id);
        if (!customer.isPresent()){
            throw new CustomerException(CustomerError.CUSTOMER_NOTFOUND);
        }
        Customer deleteCustomer =customer.get();
        customerRepository.delete(deleteCustomer);
        return "success";
    }

    @Override
    public CustomerDTO detail(Integer id) {
        if (customerRepository.findById(id).isEmpty()){
            throw new CustomerException(CustomerError.CUSTOMER_NOTFOUND);
        }
        CustomerDTO customerDTO = new CustomerDTO();
        BeanUtils.copyProperties(customerDTO,this.customerRepository.findById(id));
        return customerDTO;
    }

    @Override
    public Customer getCustomer(User user) {
        return customerRepository.getCustomerByUser(user);
    }

}

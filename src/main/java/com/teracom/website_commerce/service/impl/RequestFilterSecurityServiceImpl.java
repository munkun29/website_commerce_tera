package com.teracom.website_commerce.service.impl;

import com.google.gson.JsonObject;
import com.teracom.website_commerce.constant.HeaderKey;
import com.teracom.website_commerce.exception.CommonException;
import com.teracom.website_commerce.model.User;
import com.teracom.website_commerce.service.JwtTokenService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.util.Strings;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Service;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Log4j2
@Service
@RequiredArgsConstructor
public class RequestFilterSecurityServiceImpl extends OncePerRequestFilter {
    private final JwtTokenService jwtTokenService;
    private final UserDetailServiceImpl userDetailService;

    /**
     * @param request
     * @param response
     * @param filterChain
     * @action filter request before forward controller(@Controller|@RestController)
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws IOException {
        String path = request.getRequestURI();
        try {
            String token = request.getHeader(HeaderKey.AUTHORIZATION);
            if (Strings.isBlank(token) || path.endsWith("authenticate")) {
                filterChain.doFilter(request, response);
                return;
            }
            token = token.replace("Bearer", "").trim();
            log.info("Request filter token: {}", token);
            authorized( jwtTokenService.getEmailFromJwtToken(token), token, request);
            filterChain.doFilter(request, response);
        } catch (CommonException ex) {
            log.error("Exception request filter: {}", ex.getMessage(), ex);
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("code", ex.getCode());
            jsonObject.addProperty("message", ex.getMessage());
            jsonObject.addProperty("status", ex.getHttpStatus());
            response.setContentType("application/json");
            response.setStatus(ex.getHttpStatus());
            response.getOutputStream().println(jsonObject.toString());
            log.debug("----------------Authentication Request Filter: {}-------------", jsonObject);
        } catch (Exception ex) {
            log.error("Exception request filter: {}", ex.getMessage(), ex);
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("message", ex.getMessage());
            jsonObject.addProperty("status",500);

            response.setStatus(500);
            response.setContentType("application/json");
            response.getOutputStream().println(jsonObject.toString());
            log.debug("----------------Authentication Request Filter: {}-------------", jsonObject);
        } finally {
            log.info("Request path: {}", path);
        }
    }

    /**
     * @param email
     * @param token
     * @param request
     */
    private void authorized(String email, String token, HttpServletRequest request) {
        if (Strings.isBlank(email))
        {
            return;}
        User user = userDetailService.loadUserByEmail(email);
        boolean valid = jwtTokenService.validateToken(request, token, email);
        if (valid && SecurityContextHolder.getContext().getAuthentication() == null) {
            List<GrantedAuthority> authorities = new ArrayList<>();
            authorities.add(new SimpleGrantedAuthority("ROLE_" + user.getRole()));
            var authToken = new UsernamePasswordAuthenticationToken(user, user.getPassword(), authorities);
            authToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
            SecurityContextHolder.getContext().setAuthentication(authToken);
        }
    }
}

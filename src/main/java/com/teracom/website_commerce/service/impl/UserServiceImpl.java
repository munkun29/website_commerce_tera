package com.teracom.website_commerce.service.impl;

import com.teracom.website_commerce.dto.Token;
import com.teracom.website_commerce.dto.request.RegisterForm;
import com.teracom.website_commerce.exception.UserException;
import com.teracom.website_commerce.exception.enumarate.UserError;
import com.teracom.website_commerce.model.Customer;
import com.teracom.website_commerce.model.User;
import com.teracom.website_commerce.repository.CustomerRepository;
import com.teracom.website_commerce.repository.UserRepository;
import com.teracom.website_commerce.service.JwtTokenService;
import com.teracom.website_commerce.service.UserService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.BeanUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;


@Log4j2
@Service
public class UserServiceImpl implements UserService{
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtTokenService jwtTokenService;
    private final CustomerRepository customerRepository;



    public UserServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder, JwtTokenService jwtTokenService, CustomerRepository customerRepository) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.jwtTokenService = jwtTokenService;
        this.customerRepository = customerRepository;
    }

    @Override
    public Token register(HttpServletRequest request, RegisterForm form) {
        log.debug(form);
        User newlyUser = new User();
        if (userRepository.existsByEmail(form.getEmail())) {
            throw new UserException(UserError.USER_EXISTS);
        }
        BeanUtils.copyProperties(form,newlyUser);
        newlyUser.setPassword(passwordEncoder.encode(newlyUser.getPassword()));
        log.debug(newlyUser.getPassword());
        newlyUser.setRole("user");
        newlyUser.setStatus(true);
        this.userRepository.save(newlyUser);

        String token = this.jwtTokenService .generateToken(request, newlyUser);

        Customer newlyCustomer = new Customer();
        newlyCustomer.setUser(newlyUser);
        BeanUtils.copyProperties(form,newlyCustomer);
        customerRepository.save(newlyCustomer);
        userRepository.save(newlyUser);

        return new Token(token);
    }


    @Override
    public User getCurrentUser() {
        String email = SecurityContextHolder.getContext().getAuthentication().getName();
        if (!userRepository.existsByEmail(email))
            throw new UserException(UserError.CURRENT_USER_INVALID);
        return userRepository.findByEmail(email);


    }

    @Override
    public String delete(int id) {
        if(userRepository.findById(id).isEmpty())
            throw new UserException(UserError.USER_NOTFOUND);
        userRepository.delete(userRepository.findById(id).get());
        return "Success";
    }
}

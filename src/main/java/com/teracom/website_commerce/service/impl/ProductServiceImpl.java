package com.teracom.website_commerce.service.impl;

import com.teracom.website_commerce.model.Product;
import com.teracom.website_commerce.repository.ProductRepository;
import com.teracom.website_commerce.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Override
    public List<Product> findAll() {
        return productRepository.findAll();
    }

    @Override
    public List<Product> findAll(Sort sort) {
        return productRepository.findAll(sort);
    }

    @Override
    public Page<Product> findAll(Pageable pageable) {
        return productRepository.findAll(pageable);
    }

    @Override
    public <S extends Product> S save(S entity) {
        return productRepository.save(entity);
    }

    @Override
    public Optional<Product> findById(Integer integer) {
        return productRepository.findById(integer);
    }

    @Override
    public boolean existsById(Integer integer) {
        return productRepository.existsById(integer);
    }

    @Override
    public long count() {
        return productRepository.count();
    }

    @Override
    public void deleteById(Integer integer) {
        productRepository.deleteById(integer);
    }

    @Override
    public void delete(Product entity) {
        productRepository.delete(entity);
    }

    @Override
    public void deleteAll() {
        productRepository.deleteAll();
    }

    @Override
    @Query(value = "select * from product u where u.name like CONCAT('%',?1,'%')", nativeQuery = true)
    public List<Product> searchProduct(String query) {
        return productRepository.searchProduct(query);
    }

    @Override
    @Query(value = "update product set status = 0 where id = ?1", nativeQuery = true)
    @Modifying
    public void softDelete(Integer id) {
        productRepository.softDelete(id);
    }

    @Override
    public List<Product> findAllByOrderByPriceAsc() {
        return productRepository.findAllByOrderByPriceAsc();
    }

    @Override
    public Page<Product> findByStatusTrue(Pageable pageable) {
        return productRepository.findByStatusTrue(pageable);
    }

    @Override
    public Optional<Product> findProductByIdAndStatusTrue(Integer id) {
        return productRepository.findProductByIdAndStatusTrue(id);
    }

    @Override
    public List<Product> findAllByStatusIsTrueOrderByPriceAsc() {
        return productRepository.findAllByStatusIsTrueOrderByPriceAsc();
    }
}

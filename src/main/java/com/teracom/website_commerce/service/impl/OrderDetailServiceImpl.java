package com.teracom.website_commerce.service.impl;

import com.teracom.website_commerce.common.model.TopProduct;
import com.teracom.website_commerce.model.OrderDetail;
import com.teracom.website_commerce.repository.OrderDetailRepository;
import com.teracom.website_commerce.service.OrderDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service
public class OrderDetailServiceImpl  implements OrderDetailService {

    @Autowired
    private OrderDetailRepository orderDetailRepository;

    @Override
    public List<OrderDetail> findAll() {
        return orderDetailRepository.findAll();
    }

    @Override
    public List<OrderDetail> findAll(Sort sort) {
        return orderDetailRepository.findAll(sort);
    }

    @Override
    public <S extends OrderDetail> S saveAndFlush(S entity) {
        return orderDetailRepository.saveAndFlush(entity);
    }

    @Override
    public Page<OrderDetail> findAll(Pageable pageable) {
        return orderDetailRepository.findAll(pageable);
    }

    @Override
    public <S extends OrderDetail> S save(S entity) {
        return orderDetailRepository.save(entity);
    }

    @Override
    public Optional<OrderDetail> findById(Integer integer) {
        return orderDetailRepository.findById(integer);
    }

    @Override
    public boolean existsById(Integer integer) {
        return orderDetailRepository.existsById(integer);
    }

    @Override
    public long count() {
        return orderDetailRepository.count();
    }

    @Override
    public void deleteById(Integer integer) {
        orderDetailRepository.deleteById(integer);
    }

    @Override
    public void delete(OrderDetail entity) {
        orderDetailRepository.delete(entity);
    }

    @Override
    @Query(name = "topProduct", nativeQuery = true)
    public List<TopProduct> listProduct() {
        return orderDetailRepository.listProduct();
    }

    @Override
    @Query(name = "lowProduct", nativeQuery = true)
    public List<TopProduct> sellSlowlyProduct() {
        return orderDetailRepository.sellSlowlyProduct();
    }
}

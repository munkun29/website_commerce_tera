package com.teracom.website_commerce.exception;


import com.teracom.website_commerce.exception.enumarate.AuthError;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
public class CommonException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    private String code;
    private String message;
    private Integer httpStatus;

    public CommonException(AuthError error) {
        this(error.getCode(), error.getDescription(), error.getHttpStatus());
    }

    public CommonException(String code, String message, Integer httpStatus) {
        super(message);
        this.code = code;
        this.message = message;
        this.httpStatus = httpStatus;
    }
}

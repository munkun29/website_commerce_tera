package com.teracom.website_commerce.exception;

import com.teracom.website_commerce.exception.enumarate.UserError;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserException extends CommonException {
    public UserException(UserError error) {
        super(error.getCode(), error.getDescription(), error.getHttpStatus());
    }
}

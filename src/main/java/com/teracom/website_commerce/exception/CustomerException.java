package com.teracom.website_commerce.exception;

import com.teracom.website_commerce.exception.enumarate.CustomerError;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomerException extends CommonException{
    public CustomerException(CustomerError error) {
        super(error.getCode(), error.getDescription(), error.getHttpStatus());
    }
}

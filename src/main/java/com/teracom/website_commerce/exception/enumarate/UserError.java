package com.teracom.website_commerce.exception.enumarate;

import lombok.Getter;
import org.springframework.http.HttpStatus;

/**
 * @author Anh Tran Viet
 * @email tranvietanh190196@gmail.com
 * @created 23:07 10/30/21
 */
@Getter
public enum UserError {
    USER_NOTFOUND("USER_NOTFOUND", "User not found", 404),
    USER_EXISTS("USER_EXISTS", "User already exists",HttpStatus.CONFLICT.value()),
    EMAIL_EXISTS ("EMAIL_EXISTS","Email already exists", HttpStatus.CONFLICT.value()),
    CURRENT_USER_INVALID("CURRENT_USER_INVALID", "Current user invalid", 403),
    INCORRECT_PASSWORD("INCORRECT_PASSWORD","Incorrect current password",HttpStatus.CONFLICT.value()),
    EMAIL_NOTFOUND("EMAIL_NOTFOUND","Email not found",HttpStatus.NOT_FOUND.value()),
    EMAIL_INVALID("EMAIL_INVALID", "Email not found", HttpStatus.FORBIDDEN.value());
    private final String code;
    private final String description;
    private final int httpStatus;

    UserError(String code, String description, int httpStatus) {
        this.code = code;
        this.description = description;
        this.httpStatus = httpStatus;
    }
}

package com.teracom.website_commerce.exception.enumarate;

import lombok.Getter;
@Getter
public enum AuthError {

    UNAUTHORIZED("UNAUTHORIZED", "Access is denied", 401),
    BAD_CREDENTIALS("BAD_CREDENTIALS", "Wrong username or password", 401),
    FORBIDDEN("FORBIDDEN", "Forbidden", 403),
    INVALID_TOKEN("INVALID_TOKEN", "Invalid token", 401),
    EXPIRED_TOKEN("EXPIRED_TOKEN", "Token has expired", 401),
    ACCOUNT_DISABLED("ACCOUNT_DISABLED", "Account is disabled", 423),
    ACCOUNT_EXPIRED("ACCOUNT_EXPIRED", "Account has expired", 405),
    PASSWORD_EXPIRED("PASSWORD_EXPIRED", "Password has expired", 405),
    ACCOUNT_LOCKED("ACCOUNT_LOCKED", "Account is locked", 423);

    private final String code;
    private final String description;
    private final int httpStatus;

    AuthError(String code, String description, int httpStatus) {
        this.code = code;
        this.description = description;
        this.httpStatus = httpStatus;
    }

    public static AuthError get(String errorCode) {
        AuthError[] var1 = values();
        int var2 = var1.length;

        for (int var3 = 0; var3 < var2; ++var3) {
            AuthError error = var1[var3];
            if (error.description.equalsIgnoreCase(errorCode) || error.code.equalsIgnoreCase(errorCode)) {
                return error;
            }
        }
        return null;
    }

}

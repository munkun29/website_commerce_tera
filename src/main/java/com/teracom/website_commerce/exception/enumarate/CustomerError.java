package com.teracom.website_commerce.exception.enumarate;

import lombok.Getter;
import lombok.Setter;

@Getter
public enum CustomerError {
    CUSTOMER_NOTFOUND("CUSTOMER_NOTFOUND", "Customer not found", 404);
    private final String code;
    private final String description;
    private final int httpStatus;

    CustomerError(String code, String description, int httpStatus) {
        this.code = code;
        this.description = description;
        this.httpStatus = httpStatus;
    }
}

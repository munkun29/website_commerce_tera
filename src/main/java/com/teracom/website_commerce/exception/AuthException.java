package com.teracom.website_commerce.exception;


import com.teracom.website_commerce.exception.enumarate.AuthError;

public class AuthException extends CommonException{
    public AuthException(AuthError error) {
        super(error.getCode(), error.getDescription(), error.getHttpStatus());
    }
}

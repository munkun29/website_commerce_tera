package com.teracom.website_commerce.constant;

public interface JwtClaimKey {
    String USER_ID = "user_id";
    String ROLE = "role";
}
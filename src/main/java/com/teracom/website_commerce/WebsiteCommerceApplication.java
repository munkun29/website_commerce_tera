package com.teracom.website_commerce;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.UserDetailsServiceAutoConfiguration;

@SpringBootApplication(exclude = {
        UserDetailsServiceAutoConfiguration.class
})
public class WebsiteCommerceApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebsiteCommerceApplication.class, args);
    }

}

package com.teracom.website_commerce.dto.request;

import lombok.Data;

@Data
public class MailForm {
    private String mailTo;
    private String subject;
    private String content;
}

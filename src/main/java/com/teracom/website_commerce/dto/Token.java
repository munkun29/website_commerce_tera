package com.teracom.website_commerce.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.teracom.website_commerce.utils.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Token implements Serializable {

    @JsonProperty("token")
    private String value;

    @Override
    public String toString() {
        return JsonUtil.toJson(this);
    }
}

package com.teracom.website_commerce.dto.model;

import com.teracom.website_commerce.model.Brand;
import com.teracom.website_commerce.model.Category;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductDto {

    @NotBlank(message = "name is not value")
    private String name;
    private Boolean status;
    private Double price;
    private String image;
    private Date createAt;
    private Date updatedAt;
    private String description;
    private Category category;
    private Brand brand;
}

package com.teracom.website_commerce.dto.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BrandDto {

    @NotBlank(message = "name is not value")
    private String name;
    private Boolean status = true;

}

package com.teracom.website_commerce.dto.response;

import com.teracom.website_commerce.model.Order;
import com.teracom.website_commerce.model.User;
import lombok.Getter;
import lombok.Setter;


@Setter
@Getter
public class CustomerDTO {
    private Integer id;

    private String phone;

    private String address;

    private String name;

    private User user;

    private Order order;
}

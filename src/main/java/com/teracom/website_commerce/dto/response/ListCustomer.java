package com.teracom.website_commerce.dto.response;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ListCustomer {
    private int totalItem;
    private List<CustomerDTO> customerList;
}

package com.teracom.website_commerce.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDTO {
    private String email;
    private String name;
    private String address;
    private String phone;
}

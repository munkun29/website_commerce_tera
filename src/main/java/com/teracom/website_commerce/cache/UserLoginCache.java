package com.teracom.website_commerce.cache;

import com.teracom.website_commerce.dto.request.InfoLogin;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


@Service
public class UserLoginCache implements ICache<String, InfoLogin> {
    private final Map<String, InfoLogin> cache = new ConcurrentHashMap<>();
    @Override
    public InfoLogin put(String email, InfoLogin infoLogin) {
        return cache.put(email, infoLogin);
    }

    @Override
    public InfoLogin get(String email) {
        return cache.get(email);
    }
}

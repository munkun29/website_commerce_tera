package com.teracom.website_commerce.cache;

public interface ICache <K, V>{
    V put(K k, V v);
    V get(K k);
}

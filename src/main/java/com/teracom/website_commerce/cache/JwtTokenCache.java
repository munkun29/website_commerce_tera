package com.teracom.website_commerce.cache;

import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class JwtTokenCache implements ICache<String, String>{
    private final Map<String, String> cache = new ConcurrentHashMap<>();

    @Override
    public String put(String email, String token) {
        return cache.put(email, token);
    }

    @Override
    public String get(String email) {
        return cache.get(email);
    }
}

package com.teracom.website_commerce.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table
@Data
@AllArgsConstructor
@NoArgsConstructor
public class News {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private Boolean status;

    @Column
    private String content;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date postedAt;

    @Column
    private String title;

    @ManyToOne()
    @JoinColumn(name = "customer_id", nullable = false, referencedColumnName = "id")
    private Customer customer;

    @OneToMany(mappedBy = "news")
    private List<Comment> comments;

}

package com.teracom.website_commerce.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.teracom.website_commerce.common.model.TopUser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "orders")
@Data
@AllArgsConstructor
@NoArgsConstructor
@NamedNativeQuery(name = "topUser",
        query = "select customer_id AS customerID, COUNT(customer_id) as total from orders GROUP BY customer_id order BY total DESC limit 3",
        resultSetMapping = "TopUser")
@SqlResultSetMapping(name = "TopUser", classes = @ConstructorResult(columns = {
        @ColumnResult(name = "customerId", type = Integer.class),
        @ColumnResult(name = "total", type = Integer.class) }, targetClass = TopUser.class

))
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private Double ammount;

    @Column
    private String address;

    @Column
    private String note;

    @Column
    private String orderStatus;

    @Column
    private Boolean status;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date createAt;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "customer_id", nullable = false, referencedColumnName = "id")
    private Customer customer;

    @JsonIgnore
    @OneToMany(mappedBy = "order", fetch = FetchType.LAZY)
    private List<OrderDetail> orderDetails;
}

package com.teracom.website_commerce.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.teracom.website_commerce.common.model.TopProduct;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table
@Data
@AllArgsConstructor
@NoArgsConstructor
@NamedNativeQueries({
        @NamedNativeQuery(name = "topProduct", query = "select product_id AS productID, SUM(quantity) AS count FROM order_detail GROUP BY product_id ORDER BY count DESC limit 3", resultSetMapping = "TopProduct"),
        @NamedNativeQuery(name = "lowProduct", query = "select product_id AS productID, SUM(quantity) AS count FROM order_detail GROUP BY product_id ORDER BY count ASC limit 3", resultSetMapping = "TopProduct") })
@SqlResultSetMapping(name = "TopProduct", classes = @ConstructorResult(targetClass = TopProduct.class, columns = {
        @ColumnResult(name = "productID", type = Integer.class), @ColumnResult(name = "count", type = Integer.class) }))
public class OrderDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private int quantity;

    @Column
    private Boolean status;


    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "order_id", nullable = false, referencedColumnName = "id")
    private Order order;

    @ManyToOne
    @JoinColumn(name = "product_id", nullable = false, referencedColumnName = "id")
    private Product product;
}

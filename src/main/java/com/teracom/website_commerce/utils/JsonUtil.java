package com.teracom.website_commerce.utils;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.teracom.website_commerce.utils.adapter.GsonLocalDateAdapter;
import com.teracom.website_commerce.utils.adapter.GsonLocalDateTimeAdapter;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;

import java.lang.reflect.Type;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.function.Function;


@Log4j2
public class JsonUtil {
    private JsonUtil() {
    }

    private static final Gson gson;

    static {
        gson = new GsonBuilder()
                .registerTypeAdapter(LocalDateTime.class, new GsonLocalDateTimeAdapter())
                .registerTypeAdapter(LocalDate.class, new GsonLocalDateAdapter())
                .setPrettyPrinting()
                .serializeSpecialFloatingPointValues()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();
    }

    public static <T> String toJson(T object) {
        try {
            return gson.toJson(object);
        } catch (Exception e) {
            log.error("Failed to string json {}", e.getMessage(), e);
            return "";
        }

    }

    public static <T> T mapper(String json, Class<T> clazz) {
        try {
            return gson.fromJson(json, clazz);
        } catch (Exception e) {
            log.error("Mapper {} to failed cause {} !", clazz, e.getMessage());
            return null;
        }
    }

    public static <T> T mapper(String json, Type type) {
        try {
            return gson.fromJson(json, type);
        } catch (Exception e) {
            log.error("Mapper {} to failed cause {} !", type, e.getMessage());
            return null;
        }
    }

    public static long getLong(String json, String key) {
        try {
            JSONObject jsonObject = new JSONObject(json);
            return jsonObject.getLong(key);
        } catch (JSONException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static <T> T get(String json, Function<JSONObject, T> function) {
        try {
            JSONObject jsonObject = new JSONObject(json);
            return function.apply(jsonObject);
        } catch (JSONException ex) {
            return null;
        }
    }
}

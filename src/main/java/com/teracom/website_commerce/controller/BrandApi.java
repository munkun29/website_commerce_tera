package com.teracom.website_commerce.controller;

import com.teracom.website_commerce.dto.model.BrandDto;
import io.swagger.annotations.Api;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "Brand")
public interface BrandApi {

    @RequestMapping(
        value ="/v1/api/brands",
        produces = MediaType.APPLICATION_JSON_VALUE,
        method = RequestMethod.GET
    )
    ResponseEntity<Object> getBrandList(
            @RequestParam(name = "page", required = false, defaultValue = "0") Integer page,
            @RequestParam(name = "size", required = false, defaultValue = ""+Integer.MAX_VALUE) Integer size
    );

    @RequestMapping(
            value = "/v1/api/brands/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE,
            method = RequestMethod.GET
    )
    ResponseEntity<Object> getBrand(
            @PathVariable Integer id);

    @RequestMapping(
            value = "/v1/api/brands",
            produces = MediaType.APPLICATION_JSON_VALUE,
            method = RequestMethod.POST
    )
    ResponseEntity<Object> postBrand(@Valid @RequestBody BrandDto brandDto,
                                     BindingResult bindingResult);

    @RequestMapping(
            value = "/v1/api/brands/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE,
            method = RequestMethod.DELETE
    )
    ResponseEntity<Object> deleteBrandById(@PathVariable Integer id);

    @RequestMapping(
            value = "/v1/api/brands/search",
            produces = MediaType.APPLICATION_JSON_VALUE,
            method = RequestMethod.GET
    )
    ResponseEntity<Object> searchBrand(@RequestParam String query);

    @RequestMapping(
            value = "/v1/api/brands/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE,
            method = RequestMethod.PUT
    )
    ResponseEntity<Object> upsertBrand(@PathVariable Integer id,
                                       @Valid @RequestBody BrandDto brandDto,
                                       BindingResult bindingResult
                                       );

    @RequestMapping(
            value = "v1/api/brands/softDelete/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE,
            method = RequestMethod.DELETE
    )
    ResponseEntity<Object> softDelete(@PathVariable Integer id);
}

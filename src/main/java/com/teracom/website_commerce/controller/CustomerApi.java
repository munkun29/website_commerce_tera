package com.teracom.website_commerce.controller;

import com.teracom.website_commerce.dto.request.CustomerForm;
import com.teracom.website_commerce.dto.request.CustomerUpdateForm;
import com.teracom.website_commerce.dto.response.CustomerDTO;
import com.teracom.website_commerce.dto.response.ListCustomer;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;

@Api(tags = "customer")
@RequestMapping("customer")
public interface CustomerApi {
    @ApiResponses(
            value = {@ApiResponse(code = 200, message = "Customer")}
    )
    @RequestMapping(
            value = "create",
            method = RequestMethod.POST
    )
    ResponseEntity<CustomerDTO> createCustomer (@RequestBody @Valid CustomerForm form) throws IOException;

    @PreAuthorize("hasAnyRole('admin')")
    @ApiResponses(
            value = {@ApiResponse(code = 200, message = "Create customer")}
    )
    @RequestMapping(
            value = "list",
            method = RequestMethod.GET
    )
    ResponseEntity<ListCustomer> listCustomer(@RequestParam(name = "page",required = false,defaultValue = "0") int page, @RequestParam(name = "size", required = false, defaultValue = ""+Integer.MAX_VALUE) int size);

    @ApiResponses(
            value = {@ApiResponse(code = 200, message = "Update customer")}
    )
    @RequestMapping(
            value = "update",
            method = RequestMethod.POST
    )
    @PreAuthorize("hasAnyRole('user')")
    ResponseEntity<CustomerDTO> updateCustomer (@RequestBody CustomerUpdateForm form);

    @ApiResponses(
            value = {@ApiResponse(code = 200, message = "delete customer")}
    )
    @RequestMapping(
            value = "delete/{id}",
            method = RequestMethod.DELETE
    )
    @PreAuthorize("hasAnyRole('admin')")
    ResponseEntity<String> delete(@PathVariable Integer id);

    @ApiResponses(
            value= {@ApiResponse(code = 200, message = "Get Detail Customer")}
    )
    @RequestMapping(
            value = "detail/{id}",
            method = RequestMethod.GET
    )
    @PreAuthorize("hasAnyRole('admin')")
    ResponseEntity<CustomerDTO> detailCustomer (@PathVariable Integer id);
}

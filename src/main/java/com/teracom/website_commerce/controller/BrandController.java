package com.teracom.website_commerce.controller;

import com.teracom.website_commerce.common.model.ApiError;
import com.teracom.website_commerce.common.model.ResponseObject;
import com.teracom.website_commerce.dto.model.BrandDto;
import com.teracom.website_commerce.model.Brand;
import com.teracom.website_commerce.service.BrandService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
public class BrandController implements BrandApi{

    @Autowired
    private BrandService brandService;

    @Autowired
    private ModelMapper modelMapper;


    @Override
    public ResponseEntity<Object> getBrandList(Integer page, Integer size) {
        try {

            Pageable pageable = PageRequest.of(page, size);
            Page<Brand> pageContent = brandService.findByStatusTrue(pageable);
            List<Brand> listBrand = pageContent.getContent();

            return ResponseEntity.status(HttpStatus.OK).body(
              new ResponseObject("query succesfully", pageContent)
            );

        } catch (Exception e){
            List<Brand> list = new ArrayList<>();

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ResponseObject("",list)
            );
        }
    }

    @Override
    public ResponseEntity<Object> getBrand(Integer id) {
        try {

            Optional<Brand> brand = brandService.findBrandByIdAndStatusTrue(id);

            return brand.isPresent() ?
                    ResponseEntity.status(HttpStatus.OK).body(
                       new ResponseObject("query successfully", brand)
                    ) :
                    ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                            new ResponseObject("brand not found", "")
                    );

        } catch (Exception e) {
            Brand brand = new Brand();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ResponseObject("",brand)
            );
        }
    }

    @Override
    public ResponseEntity<Object> postBrand(BrandDto brandDto,
                                            BindingResult bindingResult) {
        List<ApiError> errors = new ArrayList<>();
        try {

            if(bindingResult.hasErrors()){
            errors.addAll(bindingResult.getAllErrors()
                    .stream()
                    .map(p ->new ApiError(((FieldError)p)
                            .getField(),
                            p.getDefaultMessage()))
                    .collect(Collectors.toList())
                );
            }
            System.out.println(brandDto);
            boolean exist = brandService.existsBrandByName(brandDto.getName());
            if (exist) {
            errors.add(new ApiError("name", "name brand is taken"));
            }
            if(errors.size() > 0) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                        new ResponseObject("cant insert brand",errors)
                );
            }

            brandService.save(modelMapper.map(brandDto, Brand.class));

            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject("insert successfilly",brandDto)
            );

        } catch(Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ResponseObject("interal server error","")
            );
        }
    }

    @Override
    public ResponseEntity<Object> deleteBrandById(Integer id) {
        try {

        boolean exist = brandService.existsById(id);

        if(exist){

            brandService.deleteById(id);
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject("delete successfully", "")
            );

        }else{
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                    new ResponseObject("cannot find brand to delete","")
            );
        }
        } catch(Exception e) {
            Brand brand = new Brand();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ResponseObject("",brand)
            );
        }
    }

    @Override
    public ResponseEntity<Object> searchBrand(String query) {
        try {
            List<Brand> list = brandService.searchBrand(query);
            return ResponseEntity.status(HttpStatus.OK).body(
                new ResponseObject("find susccessfully", list)
        );
        } catch(Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                new ResponseObject("","")
        );
        }
    }

    @Override
    public ResponseEntity<Object> upsertBrand( Integer id,
                                               BrandDto brandDto,
                                               BindingResult bindingResult) {
        List<ApiError> errors = new ArrayList<>();
        try {
            if(bindingResult.hasErrors()){
                errors.addAll(bindingResult.getAllErrors()
                        .stream()
                        .map(p ->new ApiError(((FieldError)p)
                                .getField(),
                                p.getDefaultMessage()))
                        .collect(Collectors.toList())
                );
            }
            boolean exist = brandService.existsBrandByName(brandDto.getName());
            Optional<Brand> brandOptional = brandService.findById(id);
            if (brandOptional.isPresent()) {
                if (!brandOptional.get().getName().equalsIgnoreCase(brandDto.getName()) && exist) {
                    errors.add(new ApiError("name", "name brand is taken"));
                }
            }
            if(errors.size() > 0) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                        new ResponseObject("cant update brand", errors)
                );
            }

            Brand updatedBrand = brandService.findById(id)
                    .map(brand -> {
                        brand.setName(brandDto.getName());
                        brand.setStatus(brandDto.getStatus());
                        return brandService.save(brand);
                    }).orElseGet(() -> {
                        return brandService.save(modelMapper.map(brandDto, Brand.class));
                            }
                    );
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject("Update Brand successfully",updatedBrand)
            );
        } catch(Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ResponseObject("","")
            );
        }
    }

    @Override
    public ResponseEntity<Object> softDelete(Integer id) {
        try {
        boolean exist = brandService.existsById(id);
        if(exist){

            brandService.softDelete(id);
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject("deleted","")
            );

        }else{
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                    new ResponseObject("cannot soft delete","")
            );
        }
        } catch(Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ResponseObject("","")
            );
        }
    }


}

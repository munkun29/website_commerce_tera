package com.teracom.website_commerce.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.teracom.website_commerce.common.model.ResponseObject;
import com.teracom.website_commerce.model.Brand;
import com.teracom.website_commerce.model.Order;
import com.teracom.website_commerce.service.OrderService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@RestController
public class OrderController implements OrderApi{

    @Autowired
    private OrderService orderService;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public ResponseEntity<Object> getOrderList(Integer page, Integer size) {
        try {
            Pageable pageable = PageRequest.of(page, size);
            Page<Order> pageContent = orderService.findAll(pageable);
            List<Order> listCategory = pageContent.getContent();
            List<Order> list = orderService.findAll();
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject("query successfully", list)
            );
        } catch(Exception e){
            List<Order> list = new ArrayList<>();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ResponseObject("",list)
            );
        }
    }

    @Override
    public ResponseEntity<Object> getOrder(Integer id) {
        try{

            Optional<Order> order = orderService.findById(id);

            return order.isPresent() ?
                    ResponseEntity.status(HttpStatus.OK).body(
                            new ResponseObject("query succesfully",order)
                    ) :
                    ResponseEntity.status(HttpStatus.OK).body(
                            new ResponseObject("","")
                    );
        } catch(Exception e){
            Order order = new Order();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ResponseObject("",order)
            );
        }
    }

    @Override
    public ResponseEntity<Object> postOrder(JsonNode orderData) {
        orderService.create(orderData);
        return new ResponseEntity<>("created", HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> upsertOrder(Order newOrder, Integer id) {

        try {
            Order updateOrder = orderService.findById(id)
                    .map(order -> {
                        order.setOrderStatus(newOrder.getOrderStatus());
                        order.setStatus(newOrder.getStatus());
                        order.setCustomer(newOrder.getCustomer());
                        order.setAddress(newOrder.getAddress());
                        order.setNote(newOrder.getNote());
                        order.setUpdatedAt(newOrder.getUpdatedAt());
                        order.setCreateAt(newOrder.getCreateAt());
                        order.setAmmount(newOrder.getAmmount());
                        return orderService.save(order);
                    }).orElseGet(() -> {
                                return orderService.save(modelMapper.map(newOrder, Order.class));
                            }
                    );
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject("Update Brand successfully",updateOrder)
            );
        } catch(Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ResponseObject("","")
            );
        }
    }

    @Override
    public ResponseEntity<Object> softDelete(Integer id) {
        return null;
    }

    @Override
    public ResponseEntity<Object> deleteOrderById(Integer id) {
        return null;
    }

    @Override
    public ResponseEntity<Object> searchOrder(String query) {
        try {
            List<Order> list = orderService.searchOrder(query);
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject("find susccessfully", list)
            );
        } catch(Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ResponseObject("","")
            );
        }
    }

}

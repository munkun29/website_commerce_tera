package com.teracom.website_commerce.controller;

import com.teracom.website_commerce.dto.Token;
import com.teracom.website_commerce.dto.request.RegisterForm;
import com.teracom.website_commerce.dto.response.UserDTO;
import com.teracom.website_commerce.model.User;
import com.teracom.website_commerce.service.CustomerService;
import com.teracom.website_commerce.service.JwtTokenService;
import com.teracom.website_commerce.service.UserService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;


@RestController
@Log4j2
public class UserController implements UserApi{
    private final UserService userService;
    private final JwtTokenService jwtTokenService;
    private final CustomerService customerService;


    public UserController(UserService userService, JwtTokenService jwtTokenService, CustomerService customerService) {
        this.userService = userService;
        this.jwtTokenService = jwtTokenService;
        this.customerService = customerService;
    }

    @Override
    public ResponseEntity<Token> register(HttpServletRequest request, RegisterForm form) {
        try{
        return ResponseEntity.ok(userService.register(request, form));}
        catch (Exception e){
            return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<UserDTO> getCurrentUser() {
        try{
            User user = userService.getCurrentUser();
            UserDTO userDTO = new UserDTO();
            BeanUtils.copyProperties(user,userDTO);
            BeanUtils.copyProperties(customerService.getCustomer(user),userDTO);

        return ResponseEntity.ok(userDTO);}
        catch (Exception e){
            return new ResponseEntity(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<String> delete(Integer id) {
        try {return ResponseEntity.ok(userService.delete(id));}
        catch (Exception e){
            return new ResponseEntity(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<?> logout() {
        return new ResponseEntity(HttpStatus.OK);
    }

}

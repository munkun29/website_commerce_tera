package com.teracom.website_commerce.controller;

import com.teracom.website_commerce.dto.request.InfoLogin;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;


public interface AuthenticationApi {
    @ApiResponses(
            value = {@ApiResponse(code = 200, message = "Authentication")})
    @RequestMapping(
            value = "authentication",
            method = RequestMethod.POST)
    ResponseEntity<?> authenticate(HttpServletRequest request, @Valid @RequestBody InfoLogin infoLogin);

    @ApiResponses(
            value = {@ApiResponse(code = 200, message = "Authorized")})
    @RequestMapping(
            value = "authorized",
            method = RequestMethod.GET)
    ResponseEntity<String> authorized(HttpServletRequest request, @RequestParam("token") String token);

    void doAuthenticate(final InfoLogin login);
}

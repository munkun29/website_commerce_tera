package com.teracom.website_commerce.controller;

import com.teracom.website_commerce.common.model.ResponseObject;
import com.teracom.website_commerce.model.Brand;
import com.teracom.website_commerce.model.OrderDetail;
import com.teracom.website_commerce.service.OrderDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

public class OrderDetailController implements OrderDetailApi{

    @Autowired
    private OrderDetailService orderDetailService;
    @Override
    public ResponseEntity<Object> getAllOrderDetail(Integer page, Integer size) {
        try {

            Pageable pageable = PageRequest.of(page, size);
            Page<OrderDetail> pageContent = orderDetailService.findAll(pageable);
            List<OrderDetail> listOrderDetail = pageContent.getContent();

            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject("query succesfully", pageContent)
            );

        } catch (Exception e){
            List<Brand> list = new ArrayList<>();

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ResponseObject("",list)
            );
        }
    }

    @Override
    public ResponseEntity<Object> addOrderDetail() {
        return null;
    }

    @Override
    public ResponseEntity<Object> findOrderDetailByOrderId(Integer id) {
        return null;
    }
}

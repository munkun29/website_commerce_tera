package com.teracom.website_commerce.controller;

import com.teracom.website_commerce.dto.model.ProductDto;
import com.teracom.website_commerce.model.Product;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;




public interface ProductApi {

    @RequestMapping(
            value = "/v1/api/products",
            produces = MediaType.APPLICATION_JSON_VALUE,
            method = RequestMethod.GET
    )
    ResponseEntity<Object> getProductList(
            @RequestParam(name = "page", required = false, defaultValue = "0") Integer page,
            @RequestParam(name = "size", required = false, defaultValue = ""+Integer.MAX_VALUE) Integer size
    );

    @RequestMapping(
            value = "/v1/api/products/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE,
            method = RequestMethod.GET
    )
    ResponseEntity<Object> getProduct(@PathVariable Integer id);

    @RequestMapping(
            value = "/v1/api/products",
            produces = MediaType.APPLICATION_JSON_VALUE,
//            consumes = MediaType.MULTIPART_FORM_DATA_VALUE,
            method = RequestMethod.POST
    )
    ResponseEntity<Object> postProduct(@RequestParam String product,
                                       @RequestParam("image") MultipartFile image);

    @RequestMapping(
            value = "/v1/api/products/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE,
            method = RequestMethod.PUT
    )
    ResponseEntity<Object> upsertProduct(@RequestBody Product product, @PathVariable Integer id);

    @RequestMapping(
            value = "/v1/api/products/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE,
            method = RequestMethod.DELETE
    )
    ResponseEntity<Object> deleteProductById(@PathVariable Integer id);

    @RequestMapping(
            value = "v1/api/products/softDelete/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE,
            method = RequestMethod.DELETE
    )
    ResponseEntity<Object> softDelete(@PathVariable Integer id);

    @RequestMapping(
            value = "/v1/api/products/search",
            produces = MediaType.APPLICATION_JSON_VALUE,
            method = RequestMethod.GET
    )
    ResponseEntity<Object> searchProduct(@RequestParam String query);

    @RequestMapping(
            value = "/v1/api/products/sort",
            produces = MediaType.APPLICATION_JSON_VALUE,
            method = RequestMethod.GET
    )
    ResponseEntity<Object> sortProductByPrice();

}

package com.teracom.website_commerce.controller;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

public interface OrderDetailApi {

    @RequestMapping(
            value = "/v1/api/orderdetails",
            produces = MediaType.APPLICATION_JSON_VALUE,
            method = RequestMethod.GET
    )
    ResponseEntity<Object> getAllOrderDetail(
            @RequestParam(name = "page", required = false, defaultValue = "0") Integer page,
            @RequestParam(name = "size", required = false, defaultValue = ""+Integer.MAX_VALUE) Integer size
    );

    @RequestMapping(
            value = "/v1/api/orderdetails",
            produces = MediaType.APPLICATION_JSON_VALUE,
            method = RequestMethod.POST
    )
    ResponseEntity<Object> addOrderDetail();

    @RequestMapping(
            value = "/v1/api/orderdetails/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE,
            method = RequestMethod.GET
    )
    ResponseEntity<Object> findOrderDetailByOrderId(@PathVariable Integer id);


}

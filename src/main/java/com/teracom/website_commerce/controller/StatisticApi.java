package com.teracom.website_commerce.controller;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

public interface StatisticApi {

    @RequestMapping(
            value = "v1/api/statistic/top-product",
            produces = MediaType.APPLICATION_JSON_VALUE,
            method = RequestMethod.GET
    )
    ResponseEntity<Object> topProduct();

    @RequestMapping(
            value = "v1/api/statistic/sell-slowly-product",
            produces = MediaType.APPLICATION_JSON_VALUE,
            method = RequestMethod.GET
    )
    ResponseEntity<Object> sellslowlyProduct();

    @RequestMapping(
            value = "/v1/api/statistic/top-user",
            produces = MediaType.APPLICATION_JSON_VALUE,
            method = RequestMethod.GET
    )
    ResponseEntity<Object> topUser();
}

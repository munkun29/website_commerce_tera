package com.teracom.website_commerce.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.teracom.website_commerce.common.service.StorageService;
import com.teracom.website_commerce.common.model.ResponseObject;
import com.teracom.website_commerce.model.Product;
import com.teracom.website_commerce.service.ProductService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
public class ProductController implements ProductApi{

    @Autowired
    private ProductService productService;

    @Autowired
    private StorageService storageService;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public ResponseEntity<Object> getProductList(Integer page, Integer size) {
        try {

            Pageable pageable = PageRequest.of(page,size);
            Page<Product> pageContent = productService.findByStatusTrue(pageable);
            List<Product> list = pageContent.getContent();
           return ResponseEntity.status(HttpStatus.OK).body(
                   new ResponseObject("query successfully", list)
           );

        } catch(Exception e) {
            List<Product> list = new ArrayList<>();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ResponseObject("", list)
            );
        }
    }

    @Override
    public ResponseEntity<Object> getProduct(Integer id) {
        try {

            Optional<Product> product = productService.findProductByIdAndStatusTrue(id);
            return product.isPresent() ?
                    ResponseEntity.status(HttpStatus.OK).body(
                            new ResponseObject("query successfully", product)
                    ) :
                    ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                            new ResponseObject("product not found", "")
                    );

        } catch(Exception e) {
            Product product = new Product();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ResponseObject("internal server error", "")
            );
        }
    }

    @Override
    public ResponseEntity<Object> postProduct(String product,
                                              MultipartFile image) {

        try {
            ObjectMapper mapper = new ObjectMapper();
            System.out.println(product);
            Product p = mapper.readValue(product, Product.class);
            String newImage = storageService.storeFile(image);
            p.setImage(newImage);
            Product saveProduct = productService.save(p);
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject("insert successfully", saveProduct)
            );
        } catch(Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ResponseObject("", "")
            );
        }
    }

    @Override
    public ResponseEntity<Object> upsertProduct(Product newProduct, Integer id) {
        try {
        Product updateProduct = productService.findById(id)
                .map(brand -> {
                    brand.setName(newProduct.getName());
                    brand.setStatus(newProduct.getStatus());
                    brand.setUpdatedAt(newProduct.getUpdatedAt());
                    brand.setBrand(newProduct.getBrand());
                    brand.setCategory(newProduct.getCategory());
                    brand.setDescription(newProduct.getDescription());
                    brand.setPrice(newProduct.getPrice());
                    brand.setCreateAt(newProduct.getCreateAt());
                    brand.setImage(newProduct.getImage());
                    return productService.save(brand);
                }).orElseGet(() -> {
                    return productService.save(newProduct);
                });
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject("Update Brand successfully",updateProduct)
            );
        } catch(Exception e) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                new ResponseObject("", "")
        );
        }
    }

    @Override
    public ResponseEntity<Object> deleteProductById(Integer id) {
        try {
            boolean exist =  productService.existsById(id);

            if(exist){
                productService.deleteById(id);
                return ResponseEntity.status(HttpStatus.OK).body(
                        new ResponseObject("delete successfullt", "")
                );
            } else{
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                        new ResponseObject("cannot found product", "")
                );
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ResponseObject("","")
            );
        }
    }

    @Override
    public ResponseEntity<Object> softDelete(Integer id) {
        try {
            boolean exist = productService.existsById(id);
            if(exist){

                productService.softDelete(id);
                return ResponseEntity.status(HttpStatus.OK).body(
                        new ResponseObject("deleted","")
                );

            }else{
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                        new ResponseObject("cannot soft delete","")
                );
            }
        } catch(Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ResponseObject("","")
            );
        }
    }

    @Override
    public ResponseEntity<Object> searchProduct(String query) {
        try {
            List<Product> list = productService.searchProduct(query);
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject("query successfully", list)
            );
        } catch(Exception e) {
            List<Product> list = new ArrayList<>();
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject("internal server error", list)
            );
        }
    }

    @Override
    public ResponseEntity<Object> sortProductByPrice() {
        try {
            List<Product> list = productService.findAllByStatusIsTrueOrderByPriceAsc();
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject("query successfully", list)
            );
        } catch (Exception e){
            List<Product> list = new ArrayList<>();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ResponseObject("internal server error", list)
            );
        }
    }
}

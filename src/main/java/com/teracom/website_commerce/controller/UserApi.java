package com.teracom.website_commerce.controller;

import com.teracom.website_commerce.dto.Token;
import com.teracom.website_commerce.dto.request.RegisterForm;
import com.teracom.website_commerce.dto.response.UserDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Api(tags = "user")
@RequestMapping("user")
public interface UserApi {
    @ApiResponses(
            value = {@ApiResponse(code = 200, message = "Register")})
    @RequestMapping(
            value = "register",
            method = RequestMethod.POST)
    ResponseEntity<Token> register (HttpServletRequest request,
            @RequestBody @Valid RegisterForm form
    );

    @ApiResponses(
            value = {@ApiResponse(code = 200, message = "Current user")}
    )
    @PreAuthorize("hasAnyRole('user')")
    @RequestMapping(
            value = "current-user",
            method = RequestMethod.GET
    )
    ResponseEntity<UserDTO> getCurrentUser ();

    @ApiResponses(
            value = {@ApiResponse(code = 200, message = "Delete user")}
    )
    @RequestMapping(
            value = "delete",
            method = RequestMethod.DELETE
    )
    @PreAuthorize("hasAnyRole('admin')")
    ResponseEntity<String> delete (@PathVariable Integer id);

    @PreAuthorize("hasAnyRole('user')")
    @ApiResponses(
            value = {@ApiResponse(code = 200, message = "Logout")}
    )
    @RequestMapping(
            value = "logout",
            method = RequestMethod.POST
    )
    ResponseEntity<?> logout ();
}

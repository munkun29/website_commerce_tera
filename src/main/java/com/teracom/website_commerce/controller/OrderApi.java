package com.teracom.website_commerce.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.teracom.website_commerce.model.Order;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


public interface OrderApi {

    @RequestMapping(
            value = "/v1/api/orders",
            produces = MediaType.APPLICATION_JSON_VALUE,
            method = RequestMethod.GET
    )
    ResponseEntity<Object> getOrderList(
            @RequestParam(name = "page", required = false, defaultValue = "0") Integer page,
            @RequestParam(name = "size", required = false, defaultValue = ""+Integer.MAX_VALUE) Integer size
    );

    @RequestMapping(
            value = "/v1/api/orders/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE,
            method = RequestMethod.GET
    )
    ResponseEntity<Object> getOrder(@PathVariable Integer id);

    @RequestMapping(
            value = "/v1/api/orders",
            produces = MediaType.APPLICATION_JSON_VALUE,
            method = RequestMethod.POST
    )
    ResponseEntity<Object> postOrder(@RequestBody JsonNode orderData);

    @RequestMapping(
            value = "/v1/api/orders/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE,
            method = RequestMethod.PUT
    )
    ResponseEntity<Object> upsertOrder(
            @RequestBody Order order,
            @PathVariable Integer id
            );

    @RequestMapping(
            value = "v1/api/orders/softDelete/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE,
            method = RequestMethod.DELETE
    )
    ResponseEntity<Object> softDelete(@PathVariable Integer id);

    @RequestMapping(
            value = "/v1/api/orders/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE,
            method = RequestMethod.DELETE
    )
    ResponseEntity<Object> deleteOrderById(@PathVariable Integer id);

    @RequestMapping(
            value = "/v1/api/orders/search",
            produces = MediaType.APPLICATION_JSON_VALUE,
            method = RequestMethod.GET
    )
    ResponseEntity<Object> searchOrder(@RequestParam String query);
}

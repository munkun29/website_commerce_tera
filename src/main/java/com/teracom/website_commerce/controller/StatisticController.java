package com.teracom.website_commerce.controller;

import com.teracom.website_commerce.service.OrderDetailService;
import com.teracom.website_commerce.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StatisticController implements StatisticApi{

    @Autowired
    private OrderDetailService orderDetailService;

    @Autowired
    private OrderService orderService;

    @Override
    public ResponseEntity<Object> topProduct() {
        return new ResponseEntity<>(orderDetailService.listProduct(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> sellslowlyProduct() {
        return new ResponseEntity<>(orderDetailService.sellSlowlyProduct(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> topUser() {
        return new ResponseEntity<>(orderService.topUser(), HttpStatus.OK);
    }
}

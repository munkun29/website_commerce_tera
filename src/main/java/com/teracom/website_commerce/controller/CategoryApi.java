package com.teracom.website_commerce.controller;

import com.teracom.website_commerce.common.model.ResponseObject;
import com.teracom.website_commerce.dto.model.CategoryDto;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


public interface CategoryApi {

    @RequestMapping(
            value = "/v1/api/categories",
            produces = MediaType.APPLICATION_JSON_VALUE,
            method = RequestMethod.GET
    )
    ResponseEntity<Object> getCategoryList(
            @RequestParam(name = "page", required = false, defaultValue = "0") Integer page,
            @RequestParam(name = "size", required = false, defaultValue = ""+Integer.MAX_VALUE) Integer size
    );

    @RequestMapping(
            value = "/v1/api/categories/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE,
            method= RequestMethod.GET
    )
    ResponseEntity<Object> getCategory(@PathVariable Integer id);

    @RequestMapping(
            value = "/v1/api/categories",
            produces = MediaType.APPLICATION_JSON_VALUE,
            method = RequestMethod.POST
    )
    ResponseEntity<Object> postCategory(@Valid  @RequestBody CategoryDto categoryDto,
                                        BindingResult bindingResult);

    @RequestMapping(
            value = "/v1/api/categories/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE,
            method = RequestMethod.DELETE
    )
    ResponseEntity<Object> deleteCategoryById(@PathVariable Integer id);

    @RequestMapping(
            value = "/v1/api/categories/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE,
            method = RequestMethod.PUT
    )
    ResponseEntity<Object> upsertCategory(@RequestBody CategoryDto categoryDto,
                                          @PathVariable Integer id,
                                          BindingResult bindingResult);

    @RequestMapping(
            value = "/v1/api/categories/search",
            produces = MediaType.APPLICATION_JSON_VALUE,
            method = RequestMethod.GET
    )
    ResponseEntity<ResponseObject> searchCategory(@RequestParam String query);

    @RequestMapping(
            value = "/v1/api/categories/softDelete/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE,
            method = RequestMethod.DELETE
    )
    ResponseEntity<Object> softDelete(@PathVariable Integer id);
}

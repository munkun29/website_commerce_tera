package com.teracom.website_commerce.controller;

import com.teracom.website_commerce.common.model.ApiError;
import com.teracom.website_commerce.common.model.ResponseObject;
import com.teracom.website_commerce.dto.model.CategoryDto;
import com.teracom.website_commerce.model.Category;
import com.teracom.website_commerce.service.CategoryService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
public class CategoryController implements CategoryApi{

    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private CategoryService categoryService;

    @Override
    public ResponseEntity<Object> getCategoryList(Integer page, Integer size) {
       try {
           Pageable pageable = PageRequest.of(page, size);
           Page<Category> pageContent = categoryService.findAll(pageable);
           List<Category> listCategory = pageContent.getContent();
           return ResponseEntity.status(HttpStatus.OK).body(
                   new ResponseObject("query successfully", listCategory)
           );
       } catch(Exception e){
           List<Category> list = new ArrayList<>();
           return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ResponseObject("",list)
           );
       }
    }

    @Override
    public ResponseEntity<Object> getCategory(Integer id) {
        try{

            Optional<Category> category = categoryService.findById(id);

            return category.isPresent() ?
                    ResponseEntity.status(HttpStatus.OK).body(
                      new ResponseObject("query succesfully",category)
                    ) :
                    ResponseEntity.status(HttpStatus.OK).body(
                      new ResponseObject("","")
                    );
        } catch(Exception e){
            Category category = new Category();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ResponseObject("",category)
            );
        }
    }

    @Override
    public ResponseEntity<Object> postCategory(CategoryDto categoryDto,
                                               BindingResult bindingResult) {
        List<ApiError> errors = new ArrayList<>();
        try {
            if(bindingResult.hasErrors()){
                errors.addAll(bindingResult.getAllErrors()
                        .stream()
                        .map(p ->new ApiError(((FieldError)p)
                                .getField(),
                                p.getDefaultMessage()))
                        .collect(Collectors.toList())
                );
            }
            boolean exist = categoryService.existsCategoriesByName(categoryDto.getName());
            if (exist) {
                errors.add(new ApiError("name", "name brand is taken"));
            }
            if(errors.size() > 0) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                        new ResponseObject("cant insert category", errors)
                );
            }
            categoryService.save(modelMapper.map(categoryDto, Category.class));

            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject("insert successfilly", categoryDto)
            );

        } catch(Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ResponseObject("", "")
            );
        }
    }

    @Override
    public ResponseEntity<Object> deleteCategoryById(Integer id) {
        try {
            boolean exist = categoryService.existsById(id);
            if(!exist) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                        new ResponseObject("not found category", "")
                );
            }
            categoryService.deleteById(id);
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject("delete successfully", "")
            );
        } catch(Exception e) {
            Category category = new Category();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ResponseObject("internal server error", category)
            );
        }
    }

    @Override
    public ResponseEntity<Object> upsertCategory(CategoryDto categoryDto,
                                                 Integer id,
                                                 BindingResult bindingResult) {
        List<ApiError> errors = new ArrayList<>();
        try {
            if(bindingResult.hasErrors()){
                errors.addAll(bindingResult.getAllErrors()
                        .stream()
                        .map(p ->new ApiError(((FieldError)p)
                                .getField(),
                                p.getDefaultMessage()))
                        .collect(Collectors.toList())
                );
            }
            boolean exist = categoryService.existsCategoriesByName(categoryDto.getName());
            Optional<Category> categoryOptional = categoryService.findById(id);
            if (categoryOptional.isPresent()) {
                if (!categoryOptional.get().getName().equalsIgnoreCase(categoryDto.getName()) && exist) {
                    errors.add(new ApiError("name", "name category is taken"));
                }
            }
            if(errors.size() > 0) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                        new ResponseObject("cant update category", errors)
                );
            }

            Category updatedCategory = categoryService.findById(id)
                    .map(category -> {
                        category.setName(categoryDto.getName());
                        category.setStatus(categoryDto.getStatus());
                        return categoryService.save(category);
                    }).orElseGet(() -> {
                                return categoryService.save(modelMapper.map(categoryDto, Category.class));
                            }
                    );
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject("Update category successfully", updatedCategory)
            );
        } catch(Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ResponseObject("internal server error", "")
            );
        }
    }

    @Override
    public ResponseEntity<ResponseObject> searchCategory(String query) {
        try {
            List<Category> list = categoryService.searchCategory(query);
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject("find susccessfully", list)
            );
        } catch(Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ResponseObject("", "")
            );
        }
    }

    @Override
    public ResponseEntity<Object> softDelete(Integer id) {
        try {
            boolean exist = categoryService.existsById(id);
            if(exist){

                categoryService.softDelete(id);
                return ResponseEntity.status(HttpStatus.OK).body(
                        new ResponseObject("deleted", "")
                );

            }else{
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                        new ResponseObject("cannot soft delete", "")
                );
            }
        } catch(Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ResponseObject("", "")
            );
        }
    }


}

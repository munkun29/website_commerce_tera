package com.teracom.website_commerce.controller;


import com.teracom.website_commerce.dto.Token;
import com.teracom.website_commerce.dto.request.InfoLogin;
import com.teracom.website_commerce.exception.CommonException;
import com.teracom.website_commerce.exception.enumarate.AuthError;
import com.teracom.website_commerce.service.JwtTokenService;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.*;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;


@Log4j2
@RestController
@Api(tags = "Authentication Controller")
@RequiredArgsConstructor
public class AuthenticationController implements AuthenticationApi{
    @Autowired
    private final JwtTokenService jwtTokenService;

    @Autowired
    private final AuthenticationManager authenticationManager;

    public ResponseEntity<Token> authenticate(HttpServletRequest request, @Valid @RequestBody InfoLogin infoLogin) {
        String token = jwtTokenService.getTokenIfExist(request, infoLogin);
        if(Strings.isNotBlank(token))
            return ResponseEntity.ok(new Token(token));
        this.doAuthenticate(infoLogin);
        token = jwtTokenService.generateToken(request,infoLogin);
        return ResponseEntity.ok(new Token(token));
    }

    public ResponseEntity<String> authorized(HttpServletRequest request, @RequestParam("token") String token) {
        log.info("AUTHORIZED token: {}", token);
        if (jwtTokenService.isValid(request, token))
            throw new CommonException(AuthError.EXPIRED_TOKEN);
        String role = jwtTokenService.getRoleFromJwtToken(token);
        return ResponseEntity.ok(role);
    }


     public void doAuthenticate(final InfoLogin login) {
        String error = "none";
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(login.getEmail(), login.getPassword()));
        } catch (DisabledException ex){
            error = ex.getMessage();
            throw new CommonException(AuthError.ACCOUNT_DISABLED);
        }catch (AccountExpiredException ex){
            error = ex.getMessage();
            throw new CommonException(AuthError.ACCOUNT_EXPIRED);
        }catch (CredentialsExpiredException ex){
            error = ex.getMessage();
            throw new CommonException(AuthError.PASSWORD_EXPIRED);
        }catch (LockedException ex){
            error = ex.getMessage();
            throw new CommonException(AuthError.ACCOUNT_LOCKED);
        }catch (BadCredentialsException ex) {
            error = ex.getMessage();
            throw new CommonException(AuthError.BAD_CREDENTIALS);
        }catch (AuthenticationException ex) {
            error = ex.getMessage();
            throw new CommonException(AuthError.UNAUTHORIZED);
        }finally {
            log.info("Authenticate ---------> email: {}, error: {}", login.getEmail(), error);
        }
    }


}

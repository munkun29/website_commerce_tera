package com.teracom.website_commerce.controller;

import com.teracom.website_commerce.dto.request.CustomerForm;
import com.teracom.website_commerce.dto.request.CustomerUpdateForm;
import com.teracom.website_commerce.dto.response.CustomerDTO;
import com.teracom.website_commerce.dto.response.ListCustomer;
import com.teracom.website_commerce.service.CustomerService;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@Log4j2
public class CustomerController implements CustomerApi{
    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @Override
    public ResponseEntity<CustomerDTO> createCustomer(CustomerForm form) throws IOException {
        try{
        return ResponseEntity.ok(customerService.create(form));}
        catch (Exception e){
            return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<ListCustomer> listCustomer(int page, int size) {
       try {
           return ResponseEntity.ok(customerService.list(page,size));
       } catch(Exception e) {
        return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
       }
    }

    @Override
    public ResponseEntity<CustomerDTO> updateCustomer(CustomerUpdateForm form) {
        try {
            return ResponseEntity.ok(customerService.update(form));
        }catch (Exception e){
            return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<String> delete(Integer id) {
        try {
            return ResponseEntity.ok(customerService.delete(id));
        }catch (Exception e){
            return new ResponseEntity(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<CustomerDTO> detailCustomer(Integer id) {
        try {
            return ResponseEntity.ok(customerService.detail(id));
        }
       catch (Exception e) {
           return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
       }
    }

}
